package mylog

import (
	"io"
	"os"

	log "github.com/sirupsen/logrus"
)

var (
	//Log logrus logger
	Log *log.Logger
	//LogFile exported as needs to call Close() in main()
	LogFile *os.File
	//myFields adds additional fields to log lines
	myFields log.Fields
)

//Config interface for inj of config
type Config interface {
	LogLevel() uint32
	AppName() string
	Writer() io.Writer // returns for e.g., io.MultiWriter(os.Stdout, LogFile) //LogFile, err := os.Openfile(getwd+GetPathSlash()+"logs.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
}

//MyLog logger type
type MyLog string

//New constructor for logger
func New(cfg Config) *MyLog {
	// getwd, err := os.Getwd() //gets current working directory
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// LogFile, err := os.OpenFile(getwd+GetPathSlash()+"logs.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	// if err != nil {
	// 	log.Fatal(err)
	// }

	// mw := io.MultiWriter(os.Stdout, LogFile) // write to file as well as Stdout
	// mw := io.MultiWriter(LogFile)
	mw := cfg.Writer()

	Log = log.New()
	Log.SetOutput(mw)

	logLevel := log.Level(cfg.LogLevel())
	Log.SetLevel(logLevel)

	//adds an app field to each log line
	myFields = log.Fields{
		"app": cfg.AppName(),
	}

	// Log.SetReportCaller(true) //uncomment this to enable function logging
	// Log.SetFormatter(&log.JSONFormatter{ //JSON is preferred if using a log aggregation tool like Splunk/Logstash
	Log.SetFormatter(&log.TextFormatter{
		TimestampFormat: "Mon, 02 Jan 2006 15:04:05 +1000",
		FieldMap: log.FieldMap{
			//alphabet numbering applied as json is displayed alphabetically in log file
			log.FieldKeyLevel: "lvl",
			log.FieldKeyTime:  "time",
			log.FieldKeyMsg:   "msg",
			log.FieldKeyFunc:  "caller",
		},
		// ForceColors: true, //doesn't work on mint : TextFormatter
		PadLevelText: true, // alignment : TextFormatter
		// PrettyPrint: false, //uncomment only if using JSONFormatter
	})

	//doesn't seem to be working
	// hook, err := logrus_syslog.NewSyslogHook("udp", "localhost:514", syslog.LOG_INFO, "")
	// if err != nil {
	// 	Log.Error("Unable to connect to local syslog daemon")
	// } else {
	// 	Log.AddHook(hook)
	// 	Info("Syslog Hook applied")
	// }

	l := MyLog("my logger")
	l.Info("")
	l.Info("****************")
	l.Debug("logger initialised")
	// return Log
	return &l
}

//Info adds custom fields to log.Info
func (l MyLog) Info(args ...interface{}) {
	Log.WithFields(myFields).Info(args...)
}

//Infof adds custom fields to log.Infof
func (l MyLog) Infof(format string, args ...interface{}) {
	Log.WithFields(myFields).Infof(format, args...)
}

//Error adds custom fields to log.Info
func (l MyLog) Error(args ...interface{}) {
	Log.WithFields(myFields).Error(args...)
}

//Errorf adds custom fields to log.Infof
func (l MyLog) Errorf(format string, args ...interface{}) {
	Log.WithFields(myFields).Errorf(format, args...)
}

//Fatal adds custom fields to log.Info
func (l MyLog) Fatal(args ...interface{}) {
	Log.WithFields(myFields).Fatal(args...)
}

//Fatalf adds custom fields to log.Infof
func (l MyLog) Fatalf(format string, args ...interface{}) {
	Log.WithFields(myFields).Fatalf(format, args...)
}

//Debug adds custom fields to log.Info
func (l MyLog) Debug(args ...interface{}) {
	Log.WithFields(myFields).Debug(args...)
}

//Debugf adds custom fields to log.Infof
func (l MyLog) Debugf(format string, args ...interface{}) {
	Log.WithFields(myFields).Debugf(format, args...)
}
