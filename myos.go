package mylog

import (
	"runtime"
)

//GetPathSlash returns directory separator slashes as per the Operating System
func GetPathSlash() string {
	if runtime.GOOS == "windows" {
		return "\\"
	}
	return "/"
}
