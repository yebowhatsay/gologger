package mylog

import "testing"

func TestGetPathSlash(t *testing.T) {

	if pathSlash := GetPathSlash(); pathSlash != "/" && pathSlash != "\\" {
		t.Errorf("have %s, want either '/' or '/\\'", pathSlash)
	}
}
